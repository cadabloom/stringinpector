﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public static class StringUtil
    {
        public static char MostFrequentCharacter(this string input)
        {
            if (string.IsNullOrEmpty(input.Trim()))
                return default(char);

            IEnumerable<char> inputCharacters = input.ToLower().ToCharArray().Where(c => !Char.IsWhiteSpace(c));

            if (inputCharacters.Count() == 1)
                return inputCharacters.First();

            char mostFrequent = inputCharacters.GroupBy(character => character).OrderByDescending(character => character.Count()).FirstOrDefault().Key;
            return mostFrequent;
        }

        public static char MostFrequentCharacterFromTextFile(string path)
        {
            if (!Path.GetExtension(path).Equals(".txt", StringComparison.OrdinalIgnoreCase))
                throw new FileLoadException("Not a valid text file");

            using (StreamReader sr = new StreamReader(path))
            {
                string line = sr.ReadToEnd();
                return line.MostFrequentCharacter();
            }
        }
    }
}
