﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace StringInspector
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press 'q' to quit");
            while (true)
            {
                Console.Write("Input string: ");

                if (Console.ReadKey(true).Key == ConsoleKey.Escape)
                    break;
                   
                string input = Console.ReadLine();
                Console.WriteLine("Most frequent character: {0}", input.MostFrequentCharacter());
            }
        }
    }
}
