﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Util;
namespace StringInspector.Tests
{
    [TestClass]
    public class StringExtensionTest
    {
        [TestMethod]
        public void can_get_most_frequent_from_input_string()
        {
            char c = "dasdasdadad".MostFrequentCharacter();
            Assert.AreEqual('d', c);
        }

        [TestMethod]
        public void can_get_most_frequent_from_one_character_input()
        {
            char c = "d".MostFrequentCharacter();
            Assert.AreEqual('d', c);
        }

        [TestMethod]
        public void should_return_defalt_char_value_for_empty_string_input()
        {
            char c = "".MostFrequentCharacter();
            Assert.AreEqual('\0', c);
        }

        [TestMethod]
        public void can_get_most_frequent_from_a_large_article_text_file()
        {
            char c = StringUtil.MostFrequentCharacterFromTextFile("SampleArticle.txt");
        }

    }
}
